package com.example.drawerlayout;

public class DrawerItem {


    String ItemName;
    int imageResourceId;

    public DrawerItem(String itemName, int imgResID) {
        super();
        ItemName = itemName;
        imageResourceId = imgResID;
    }

    public String getItemName() {
        return ItemName;
    }

    public void setItemName(String itemName) {
        ItemName = itemName;
    }

    public int getImgResourceId() {
        return imageResourceId;
    }

    public void setImageResourceId(int imageResourceId) {
        this.imageResourceId = imageResourceId;
    }
}
