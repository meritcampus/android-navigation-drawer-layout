package com.example.drawerlayout;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class MainActivity extends Activity implements OnItemClickListener {
	ListView listView;
	DrawerLayout drawerLayout;
	List<DrawerItem> drawerList;
	private ActionBarDrawerToggle mDrawerToggle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		listView = (ListView) findViewById(R.id.left_menu);
		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		drawerList = new ArrayList<DrawerItem>();
		drawerList.add(new DrawerItem("Home", R.drawable.ic_action_email));
		drawerList.add(new DrawerItem("Profile", R.drawable.ic_action_good));
		drawerList.add(new DrawerItem("Order", R.drawable.ic_action_gamepad));
		drawerList.add(new DrawerItem("Search", R.drawable.ic_action_search));
		drawerList.add(new DrawerItem("History", R.drawable.ic_action_labels));
		CustomDrawerAdapter adapter = new CustomDrawerAdapter(this,
				R.layout.custom_drawer_item, drawerList);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(this);
		mDrawerToggle = createDrawerToggle();
		drawerLayout.setDrawerListener(mDrawerToggle);

	}

	private ActionBarDrawerToggle createDrawerToggle() {
		return new ActionBarDrawerToggle(this, drawerLayout,
				R.drawable.drawer_layout_icon, R.string.app_name,
				R.string.app_name) {

			@Override
			public void onDrawerClosed(View view) {
				invalidateOptionsMenu();
			}

			@Override
			public void onDrawerOpened(View drawerView) {
				invalidateOptionsMenu();
			}
		};
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		if (mDrawerToggle != null)
			mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {

		switch (position) {
		case 0:
			Toast.makeText(getApplicationContext(), "clicked on item Home",
					Toast.LENGTH_SHORT).show();
			break;
		case 1:
			Toast.makeText(getApplicationContext(), "clicked on item profile",
					Toast.LENGTH_SHORT).show();
			break;
		case 2:
			Toast.makeText(getApplicationContext(), "clicked on item order",
					Toast.LENGTH_SHORT).show();
			break;
		case 3:
			Toast.makeText(getApplicationContext(), "clicked on item search",
					Toast.LENGTH_SHORT).show();
			break;
		case 4:
			Toast.makeText(getApplicationContext(), "clicked on item History",
					Toast.LENGTH_SHORT).show();
			break;
		}

		drawerLayout.closeDrawer(listView);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
}
